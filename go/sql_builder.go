package gosqlbuilder

import "fmt"

func SqlClause(selectClause []string, fromClause string, joinClause []string) (string, []interface{}) {
	sql := "SELECT "
	selectDelimiter := ""
	for i, v := range selectClause {
		if i > 0 {
			selectDelimiter = ", "
		}
		sql = fmt.Sprintf("%s%s%s", sql, selectDelimiter, v)
	}
	sql = fmt.Sprintf("%s FROM %s", sql, fromClause)
	for _, v := range joinClause {
		sql = fmt.Sprintf("%s %s", sql, v)
	}
	return sql, nil
}

func SqlWhereClause(delimiter string, key string, operator string, value interface{}, sql string, params []interface{}) (string, []interface{}) {
	if len(params) < 1 {
		delimiter = "WHERE"
	}

	sqlValue := ""
	sqlValue, params = keyValueClause(key, operator, value, params)

	sql = fmt.Sprintf("%s %s %s", sql, delimiter, sqlValue)

	return sql, params
}

func SqlGrouping(sql string, isStart bool) string {
	if isStart {
		return fmt.Sprintf("%s (", sql)
	}
	return fmt.Sprintf("%s )", sql)
}

var specialOperator = map[string]string{
	"ANY": " = ANY ",
	"IN":  " IN ",
}

func keyValueClause(key string, operator string, value interface{}, params []interface{}) (string, []interface{}) {
	sqlValue := ""
	if v, ok := specialOperator[operator]; ok {
		switch operator {
		case "ANY":
			sqlValue, params = operatorAny(value, params)
		case "IN":
			sqlValue, params = operatorIn(value, params)
		}
		operator = v
	} else {
		sqlValue = fmt.Sprintf("$%d", len(params)+1)
		params = append(params, value)
	}

	sqlValue = fmt.Sprintf("%s%s%s", key, operator, sqlValue)

	return sqlValue, params
}

func operatorAny(value interface{}, params []interface{}) (string, []interface{}) {
	sqlValue := fmt.Sprintf("$%d", len(params)+1)
	switch val := value.(type) {
	case []string:
		sqlValue = fmt.Sprintf("(%s)", sqlValue)
		value = val
	case []int:
		sqlValue = fmt.Sprintf("(%s)", sqlValue)
		value = val
	case []int64:
		sqlValue = fmt.Sprintf("(%s)", sqlValue)
		value = val
	}
	params = append(params, value)

	return sqlValue, params
}

func operatorIn(value interface{}, params []interface{}) (string, []interface{}) {
	sqlValue := "("
	delimiter := ""
	switch val := value.(type) {
	case []string:
		for i, v := range val {
			if i > 0 {
				delimiter = ","
			}
			sqlValue = fmt.Sprintf("%s%s$%d", sqlValue, delimiter, len(params)+1)
			params = append(params, v)
		}
	case []int:
		for i, v := range val {
			if i > 0 {
				delimiter = ","
			}
			sqlValue = fmt.Sprintf("%s%s$%d", sqlValue, delimiter, len(params)+1)
			params = append(params, v)
		}
	case []int64:
		for i, v := range val {
			if i > 0 {
				delimiter = ","
			}
			sqlValue = fmt.Sprintf("%s%s$%d", sqlValue, delimiter, len(params)+1)
			params = append(params, v)
		}
	}
	sqlValue = fmt.Sprintf("%s)", sqlValue)

	return sqlValue, params
}
