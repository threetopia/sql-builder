package gosqlbuilder

import (
	"fmt"
	"testing"
)

func TestSelectClause(t *testing.T) {
	sqlResult := "SELECT mt.id, mt.column1, mt.column2 FROM main_table mt"
	sql, _ := SqlClause(
		[]string{
			"mt.id",
			"mt.column1",
			"mt.column2",
		},
		"main_table mt",
		nil,
	)
	if sql != sqlResult {
		t.Errorf("result must be (%s) got (%s)", sqlResult, sql)
	}
}

func TestSelectWithJoinClause(t *testing.T) {
	sqlResult := "SELECT mt.id, mt.column1, mt.column2 FROM main_table mt INNER JOIN inner_table it ON it.main_id=mt.id LEFT JOIN left_table lt ON lt.main_id=mt.id"
	sql, _ := SqlClause(
		[]string{
			"mt.id",
			"mt.column1",
			"mt.column2",
		},
		"main_table mt",
		[]string{
			"INNER JOIN inner_table it ON it.main_id=mt.id",
			"LEFT JOIN left_table lt ON lt.main_id=mt.id",
		},
	)
	if sql != sqlResult {
		t.Errorf("result must be (%s) got (%s)", sqlResult, sql)
	}
}

func TestSelectWithJoinWhereClause(t *testing.T) {
	sqlResult := "SELECT mt.id, mt.column1, mt.column2 FROM main_table mt INNER JOIN inner_table it ON it.main_id=mt.id LEFT JOIN left_table lt ON lt.main_id=mt.id"
	sqlResult += " WHERE mt.id=$1"
	sql, params := SqlClause(
		[]string{
			"mt.id",
			"mt.column1",
			"mt.column2",
		},
		"main_table mt",
		[]string{
			"INNER JOIN inner_table it ON it.main_id=mt.id",
			"LEFT JOIN left_table lt ON lt.main_id=mt.id",
		},
	)
	sql, params = SqlWhereClause("AND", "mt.id", "=", "qwerty", sql, params)
	if sql != sqlResult || len(params) < 1 {
		t.Errorf("result must be (%s) got (%s)", sqlResult, sql)
	}
}

func TestOutput(t *testing.T) {
	sql, params := SqlClause(
		[]string{
			"mt.id",
			"mt.column1",
			"mt.column2",
		},
		"main_table mt",
		[]string{
			"INNER JOIN inner_table it ON it.main_id=mt.id",
			"LEFT JOIN left_table lt ON lt.main_id=mt.id",
		},
	)
	sql = SqlGrouping(sql, true)
	sql, params = SqlWhereClause("AND", "mt.id", "=", "id", sql, params)
	sql, params = SqlWhereClause("AND", "mt.column1", "=", "column1", sql, params)
	sql, params = SqlWhereClause("OR", "mt.column2", "=", "column2", sql, params)
	sql = SqlGrouping(sql, false)
	sql, params = SqlWhereClause("OR", "mt.columnAny", "ANY", []string{"any1", "any2"}, sql, params)
	sql, params = SqlWhereClause("OR", "mt.columnIn", "IN", []string{"in1", "in2"}, sql, params)
	fmt.Println(sql, params)

}
